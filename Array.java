class App {

    public static void main(String[] args) {

        int ceros = 0, noCeros = 0;
        
        int[] array = new int[10];

        for (int i = 0; i < 10; i++) {
            array[i] = i;
        }
		
		for (int i = 0; i < 10; i+=2) {
            array[i] = 0;
        }

        for (int num : array) {
            System.out.println(num);
        }

        for (int num : array) {
            if (num == 0) {
                ceros++;
            } else {
                noCeros++;
            }
        }

        System.out.println("Hay " + ceros + " ceros");
        System.out.println("Hay " + noCeros + " números que no son cero");

    }

}